# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from portmod.globals import env
from portmod.lock import exclusive_lock
from portmod.merge import merge
from portmod.query import query
from portmodlib.l10n import l10n
from portmodlib.portmod import multi_select_prompt

from .merge import CLIMerge


def search_main(args):
    pkgs = list(
        query(
            " ".join(args.query),
        )
    )

    if env.INTERACTIVE:
        try:
            selection = selection_prompt([pkg.cpn for pkg in pkgs])
        except EOFError:
            return
        if selection:
            with exclusive_lock():
                merge(selection, io=CLIMerge())


def selection_prompt(atoms):
    mod_selection = multi_select_prompt("Packages to install", atoms)
    return [index for index in mod_selection]


def add_search_parser(subparsers, parents):
    parser = subparsers.add_parser("search", help=l10n("search-help"), parents=parents)

    parser.add_argument(
        "query",
        nargs="+",
        metavar=l10n("query-placeholder"),
        help=l10n("search-query-help"),
    )
    parser.set_defaults(func=search_main)
